For now, arm-autonomy-host-image-minimal installs the dhcp-server package and
the dchp recipe was copied from oe-core tree https://git.openembedded.org/openembedded-core/tree/meta/recipes-connectivity/dhcp?id=087e4fafeef82cfd3d71402d6b200fe831f48697
since it got removed in the https://git.openembedded.org/openembedded-core/commit/meta/recipes-connectivity?id=7e3357892f204788162747e907d68f857118cf42 patch.
