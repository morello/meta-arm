DESCRIPTION = "The LLVM Compiler Infrastructure"
HOMEPAGE = "http://llvm.org"
LICENSE = "Apache-2.0-with-LLVM-exception"

PROVIDES = "virtual/aarch64-c64-llvm"

LIC_FILES_CHKSUM = "file://clang-current/include/llvm/Support/LICENSE.TXT;md5=986c03435151a0086b8aaac964939cdd"

PV = "11.0.4"

SRC_URI = "https://git.morello-project.org/morello/llvm-project/-/jobs/artifacts/morello/master/raw/morello-clang.tar.xz?job=build-toolchain;downloadfilename=morello-clang.tar.xz \
          "
SHASUM_URI = "https://git.morello-project.org/morello/llvm-project/-/jobs/artifacts/morello/master/raw/SHA256SUMS.txt?job=build-toolchain"

do_fetch[prefuncs] += "fetch_checksums"
python fetch_checksums() {
    import urllib
    for line in urllib.request.urlopen(d.getVar("SHASUM_URI")):
        (sha, filename) = line.decode("ascii").strip().split()
        if filename == "morello-clang.tar.xz":
            d.setVarFlag("SRC_URI", "sha256sum", sha)
            return
    bb.error("Could not find remote checksum")
}

do_unpack[depends] += "xz-native:do_populate_sysroot"

S = "${WORKDIR}"

FILES_${PN} = "/clang-morello"
SYSROOT_DIRS += "/clang-morello"

do_patch() {
    find ${S} -type f -name *.py | xargs sed -i -e 's|/usr/bin/env python$|/usr/bin/env python3|'
    find ${S} -type f -name clang | xargs sed -i -e 's|/usr/bin/env python$|/usr/bin/env python3|'
}

do_install() {
    mkdir ${D}/clang-morello
    cp -rf ${S}/clang-current/* ${D}/clang-morello
}

INSANE_SKIP_${PN} = "already-stripped libdir staticdev file-rdeps arch dev-so rpaths useless-rpaths"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
