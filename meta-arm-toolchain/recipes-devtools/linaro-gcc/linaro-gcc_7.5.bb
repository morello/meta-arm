DESCRIPTION = "The linaro gcc Compiler Infrastructure"
HOMEPAGE = "https://releases.linaro.org"
LICENSE = "GPL-3.0-with-GCC-exception & GPLv3"
LIC_FILES_CHKSUM = "file://share/doc/gcc/GNU-Free-Documentation-License.html;md5=bc7fad4bba98e7a4cd5ab3042506493c"

PROVIDES = "virtual/aarch64-gnu-linaro"

PV = "7.5.0"

SRC_URI = "https://releases.linaro.org/components/toolchain/binaries/7.5-2019.12/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz"

SRC_URI[sha256sum] = "3b6465fb91564b54bbdf9578b4cc3aa198dd363f7a43820eab06ea2932c8e0bf"

EXCLUDE_FROM_SHLIBS = "1"

S = "${WORKDIR}/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu"

FILES_${PN} = "/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu"
SYSROOT_DIRS += "/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu"

do_install() {
    cp -rf ${S} ${D}/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu
}

INSANE_SKIP_${PN} = "already-stripped libdir staticdev file-rdeps arch dev-so ldflags"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
