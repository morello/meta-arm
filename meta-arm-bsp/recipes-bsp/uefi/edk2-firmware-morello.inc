SRC_URI = "\
           gitsm://git.morello-project.org/morello/edk2.git;protocol=https;name=edk2;destsuffix=git/edk2;branch=morello/master \
           gitsm://git.morello-project.org/morello/edk2-platforms.git;protocol=https;name=edk2-platforms;destsuffix=git/edk2/edk2-platforms;branch=morello/master \
"

# TAGS and commit ID for morello
SRCREV_edk2           = "${AUTOREV}"
SRCREV_edk2-platforms = "${AUTOREV}"

ENABLE_MORELLO_CAP ?= "1"

LLVM_PATH = "${RECIPE_SYSROOT}/clang-morello/bin"
DEPENDS += "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', 'virtual/aarch64-c64-llvm', '', d)}"
TOOL_CHAIN  = "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', 'clang', 'gcc',d)}"
EDK2_BUILD_FLAGS   += "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', '-D ENABLE_MORELLO_CAP', '', d)}"

inherit python3native
DEPENDS += "python3-native"
do_patch[noexec] = "1"
PV = "git${SRCPV}"
