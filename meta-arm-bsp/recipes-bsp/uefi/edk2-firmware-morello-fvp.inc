require edk2-firmware-morello.inc

# morello-fvp specific EDK2 configurations
EDK2_BUILD_RELEASE = "0"
EDK2_PLATFORM      = "morellofvp"
EDK2_PLATFORM_DSC  = "Morello/MorelloPlatformFvp.dsc"
EDK2_BIN_NAME      = "BL33_AP_UEFI.fd"
EDK2_ARCH          = "AARCH64"

COMPATIBLE_MACHINE = "morello-fvp"
