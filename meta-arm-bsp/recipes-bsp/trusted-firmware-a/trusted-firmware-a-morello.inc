# Morello specific TFA support

TFA_PLATFORM       = "morello"
TFA_BUILD_TARGET   = "bl31 dtbs"
TFA_DEBUG          = "1"
TFA_MBEDTLS        = "0"
TFA_UBOOT          = "0"

ENABLE_MORELLO_CAP ?= "1"

S = "${WORKDIR}/git"

SRC_URI = " \
            git://git.morello-project.org/morello/trusted-firmware-a.git;protocol=https;name=tfa;branch=morello/master \
          "
SRCREV_tfa = "${AUTOREV}"
LIC_FILES_CHKSUM = "file://docs/license.rst;md5=189505435dbcdcc8caa63c46fe93fa89"

LLVM_PATH = "${RECIPE_SYSROOT}/clang-morello/bin"
DEPENDS += "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', 'virtual/aarch64-c64-llvm', '', d)}"
ARM_TF_ARCH = "aarch64"

INSANE_SKIP_${PN} += "ldflags textrel"

EXTRA_OEMAKE += "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', '${CLANG_EXTRA_OEMAKE}', '', d)}"
EXTRA_OEMAKE += "${@bb.utils.contains('ENABLE_MORELLO_CAP', '1', 'ENABLE_MORELLO_CAP=1', 'ENABLE_MORELLO_CAP=0', d)}"
