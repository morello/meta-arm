require trusted-firmware-a-morello.inc

COMPATIBLE_MACHINE = "morello-fvp"

TFA_INSTALL_TARGET += "morello-fvp"

PV = "2.3+git${SRCPV}"
