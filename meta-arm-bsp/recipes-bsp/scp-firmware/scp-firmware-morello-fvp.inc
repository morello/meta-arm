require scp-firmware-morello.inc

COMPATIBLE_MACHINE = "morello-fvp"

FW_INSTALL = "ramfw_fvp romfw"

do_install_append() {

   fiptool \
       create \
       --scp-fw "${D}/firmware/scp_ramfw_fvp.bin" \
       --soc-fw "${RECIPE_SYSROOT}/firmware/bl31.bin" \
       "${D}/firmware/scp_fw.bin"

   fiptool \
       create \
       --blob uuid=54464222-a4cf-4bf8-b1b6-cee7dade539e,file="${D}/firmware/mcp_ramfw_fvp.bin" \
       "${D}/firmware/mcp_fw.bin"

   ln -sf "scp_romfw.bin" "${D}/firmware/scp_rom.bin"
   ln -sf "mcp_romfw.bin" "${D}/firmware/mcp_rom.bin"
}
