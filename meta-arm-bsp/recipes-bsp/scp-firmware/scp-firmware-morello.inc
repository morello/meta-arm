# Morello specific SCP configurations and build instructions

LIC_FILES_CHKSUM = "file://license.md;beginline=5;md5=9db9e3d2fb8d9300a6c3d15101b19731 \
                    file://contrib/cmsis/git/LICENSE.txt;md5=e3fc50a88d0a364313df4b21ef20c29e"

SRC_URI = "gitsm://git.morello-project.org/morello/scp-firmware.git;protocol=https;branch=morello/master"
SRCREV        = "${AUTOREV}"

SCP_PLATFORM  = "morello"
SCP_LOG_LEVEL = "INFO"

PV = "2.7.0+git${SRCPV}"

DEPENDS += "fiptool-native"
DEPENDS += "virtual/trusted-firmware-a"
