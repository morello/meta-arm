# Common machine configurations for morello board

#@TYPE: Machine
#@NAME: Morello machine
#@DESCRIPTION: Common machine configurations for morello boards

require conf/machine/include/tune-neoversen1.inc

MACHINEOVERRIDES =. "morello:"

# SCP firmware v2.7 version
PREFERRED_VERSION_scp-firmware ?= "2.7%"

# Trusted firmware v2.3 version
PREFERRED_PROVIDER_virtual/trusted-firmware-a ?= "trusted-firmware-a"
PREFERRED_VERSION_trusted-firmware-a ?= "2.3%"

EXTRA_IMAGEDEPENDS += "virtual/trusted-firmware-a"
EXTRA_IMAGEDEPENDS += "virtual/control-processor-firmware"

#UEFI EDK2 firmware
EXTRA_IMAGEDEPENDS += "virtual/uefi-firmware"

#grub-efi
EFI_PROVIDER ?= "grub-efi"
MACHINE_FEATURES += "efi"

# Use kernel provided by meta-kernel
KERNEL_IMAGETYPE ?= "Image"
PREFERRED_PROVIDER_virtual/kernel ?= "linux-yocto"
PREFERRED_VERSION_linux-yocto ?= "5.4%"
SERIAL_CONSOLES = "115200;ttyAMA0"

IMAGE_FSTYPES += "wic"
IMAGE_BOOT_FILES ?= "${MACHINE}.dtb"
WKS_FILE ?= "morello-efidisk.wks"
WKS_FILE_DEPENDS_append = " ${EXTRA_IMAGEDEPENDS}"
